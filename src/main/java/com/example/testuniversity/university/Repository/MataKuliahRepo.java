package com.example.testuniversity.university.Repository;

import com.example.testuniversity.university.Entity.MataKuliah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MataKuliahRepo extends JpaRepository<MataKuliah, Long> {
}
