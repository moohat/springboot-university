package com.example.testuniversity.university.Repository;

import com.example.testuniversity.university.Entity.Nilai;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NilaiRepo extends JpaRepository<Nilai, Long> {
}

