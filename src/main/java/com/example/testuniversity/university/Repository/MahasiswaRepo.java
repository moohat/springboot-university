package com.example.testuniversity.university.Repository;

import com.example.testuniversity.university.Entity.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MahasiswaRepo extends JpaRepository<Mahasiswa, Long> {
}