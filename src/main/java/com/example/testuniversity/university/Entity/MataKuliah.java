package com.example.testuniversity.university.Entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter //ini gunannya lombok
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="matakuliah")
public class MataKuliah {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="matakuliah_id")
    private Long id;

    @Column(name="matakuliah_name")
    private String namaMataKuliah;

    //@ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    //@JoinColumn(name="mahasiswa_id")
    //private Mahasiswa mahasiswa;


    //@OneToMany(mappedBy ="matakuliah", cascade = CascadeType.ALL, orphanRemoval = true)
    @OneToMany(  cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name="matakuliah_id")
    private List<Nilai> nilaiList;
}

