package com.example.testuniversity.university.Entity;

import lombok.*;

import javax.persistence.*;

@Getter //ini gunannya lombok
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="nilai")
public class Nilai {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="nilai_id")
    private Long id;

    //@ManyToOne(fetch=FetchType.LAZY)
    //@JoinColumn(name="matakuliah_id")
    //private MataKuliah mataKuliah;
    private Double nilai;
    private String keterangan;
}

