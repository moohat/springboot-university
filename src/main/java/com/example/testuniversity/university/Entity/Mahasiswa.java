package com.example.testuniversity.university.Entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter //ini gunannya lombok
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="mahasiswa")
public class Mahasiswa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="mahasiswa_id")
    private Long id;

    @Column(name="mahasiswa_nama")
    private String nama;

    @Column(name="mahasiswa_alamat")
    private String alamat;

    //@OneToMany(targetEntity = MataKuliah.class,mappedBy="mahasiswa",fetch= FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @OneToMany(fetch= FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name="mahasiswa_id")
    private List<MataKuliah> mataKuliahList;
}
