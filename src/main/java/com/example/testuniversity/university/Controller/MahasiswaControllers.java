package com.example.testuniversity.university.Controller;

import com.example.testuniversity.university.Entity.Mahasiswa;
import com.example.testuniversity.university.Repository.MahasiswaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class MahasiswaControllers {
    @Autowired
    MahasiswaRepo mahasiswaRepo;

    @GetMapping("/mahasiswa")
    public List<Mahasiswa> getAllMahasiswa(){
        return mahasiswaRepo.findAll();
    }

    @GetMapping("/mahasiswa/{id}")
    public Optional<Mahasiswa> getMahasiswaById(@PathVariable("id") Long id){
        Optional<Mahasiswa> mahasiswa = mahasiswaRepo.findById(id);
        return mahasiswa;
    }

    @PostMapping("/mahasiswa")
    public Mahasiswa createMahasiswa(@RequestBody Mahasiswa mahasiswa){
        return mahasiswaRepo.save(mahasiswa);

    }
    @DeleteMapping("/mahasiswa")
    public ResponseEntity deleteMahasiswa(@RequestParam("id") Long id){
        if(!mahasiswaRepo.findById(id).isPresent()){
            ResponseEntity.badRequest().build();
        }
        mahasiswaRepo.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
