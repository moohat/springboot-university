package com.example.testuniversity.university.Controller;

import com.example.testuniversity.university.Entity.MataKuliah;
import com.example.testuniversity.university.Entity.Nilai;
import com.example.testuniversity.university.Repository.MataKuliahRepo;
import com.example.testuniversity.university.Repository.NilaiRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NilaiControllers {
    @Autowired
    NilaiRepo nilaiRepo;

    @GetMapping("/nilai")
    public List<Nilai> getAll(){
        return nilaiRepo.findAll();
    }

    @PostMapping("/")
    public Nilai save(@RequestBody Nilai nilai){
        return nilaiRepo.save(nilai);
    }


}

