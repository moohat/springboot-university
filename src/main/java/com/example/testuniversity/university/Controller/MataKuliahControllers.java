package com.example.testuniversity.university.Controller;

import com.example.testuniversity.university.Entity.MataKuliah;
import com.example.testuniversity.university.Repository.MataKuliahRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/matakuliah")
@CrossOrigin
public class MataKuliahControllers {
    @Autowired
    MataKuliahRepo mataKuliahRepo;

    @GetMapping("")
    public List<MataKuliah> getAll(){
        return mataKuliahRepo.findAll();
    }

    @PostMapping("/")
    public MataKuliah save(@RequestBody MataKuliah mataKuliah){
        return mataKuliahRepo.save(mataKuliah);
    }

}

